pub fn next_of(bytes: &[u8], offset: usize, byte: u8) -> usize {
    let mut offset = offset;
    while offset < bytes.len() && bytes[offset] != byte {
        offset += 1;
    }
    offset
}

pub fn prev_of(bytes: &[u8], offset: usize, byte: u8) -> usize {
    let mut offset = offset;
    while offset > 0 && bytes[offset] != byte {
        offset -= 1;
    }
    offset
}

pub fn bounds_of(bytes: &[u8], offset: usize, byte: u8) -> (usize, usize) {
    let off_prev = if offset == 0 { 0 } else { offset - 1 };
    let prev = prev_of(bytes, off_prev, byte);
    let next = next_of(bytes, offset, byte);

    if (prev == 0 && bytes[0] != byte) || prev == next {
        return (prev, next);
    }
    (prev + 1, next)
}

pub fn move_values(
    bytes: &mut [u8],
    dest_start: usize,
    dest_end: usize,
    src_start: usize,
    src_end: usize,
) -> usize {
    if dest_end <= dest_start || src_end <= src_start {
        return 0;
    }

    let len = std::cmp::min(dest_end - dest_start, src_end - src_start);

    if dest_start > src_start {
        for i in 0..len {
            let idx = len - 1 - i;
            bytes[dest_start + idx] = bytes[src_start + idx];
        }
        return len;
    }

    for i in 0..len {
        bytes[dest_start + i] = bytes[src_start + i];
    }

    len
}

#[cfg(test)]
#[test]
fn bounds_of_text_0() {
    let test = b"\n";
    let (s, e) = bounds_of(&test[..], 0, b'\n');
    assert_eq!(s, 0);
    assert_eq!(e, 0);
}
#[test]
fn bounds_of_text_1() {
    let test = b"0\n";
    let (s, e) = bounds_of(&test[..], 0, b'\n');
    assert_eq!(s, 0);
    assert_eq!(e, 1);
}
#[test]
fn bounds_of_text_2() {
    let test = b"0\n234";
    let (s, e) = bounds_of(&test[..], 1, b'\n');
    assert_eq!(s, 0);
    assert_eq!(e, 1);
}
#[test]
fn bounds_of_text_3() {
    let test = b"\n0\n";
    let (s, e) = bounds_of(&test[..], 1, b'\n');
    assert_eq!(s, 1);
    assert_eq!(e, 2);
}
#[test]
fn bounds_of_text_4() {
    let test = b"\n\n\n";
    let (s, e) = bounds_of(&test[..], 1, b'\n');
    assert_eq!(s, 1);
    assert_eq!(e, 1);
}
#[test]
fn bounds_of_text_5() {
    let test = b"\n\n\n";
    let (s, e) = bounds_of(&test[..], 2, b'\n');
    assert_eq!(s, 2);
    assert_eq!(e, 2);
}
#[test]
fn bounds_of_text_6() {
    let test = b"0\n234";
    let (s, e) = bounds_of(&test[..], 2, b'\n');
    assert_eq!(s, 2);
    assert_eq!(e, 5);
}

#[test]
fn next_of_simple() {
    let test = b"0123\n567";
    let next = next_of(&test[..], 0, b'\n');
    assert_eq!(next, 4);
}

#[test]
fn prev_of_simple() {
    let test = b"0123\n567";
    let prev = prev_of(&test[..], 7, b'\n');
    assert_eq!(prev, 4);
}

#[test]
fn next_of_simple_corner() {
    let test = b"0123\n567";
    let next = next_of(&test[..], 7, b'\n');
    assert_eq!(next, 8);
}

#[test]
fn next_of_simple_corner_1() {
    let test = b"0123\n567";
    let next = next_of(&test[..], 4, b'\n');
    assert_eq!(next, 4);
}

#[test]
fn prev_of_simple_corner() {
    let test = b"0123\n567";
    let prev = prev_of(&test[..], 0, b'\n');
    assert_eq!(prev, 0);
}

#[test]
fn prev_of_simple_corner_1() {
    let test = b"0123\n567";
    let prev = prev_of(&test[..], 4, b'\n');
    assert_eq!(prev, 4);
}
