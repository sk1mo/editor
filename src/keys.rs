pub const ESC: u8 = 27;
pub const BACKSPACE: u8 = 8;
pub const UP: u8 = 130;
pub const DOWN: u8 = 131;
pub const LEFT: u8 = 132;
pub const RIGHT: u8 = 133;
