#[derive(Clone, Copy, Default, Debug)]
pub struct Dim {
    pub dx: i64,
    pub dy: i64,
}

#[derive(Clone, Copy, Default, Debug)]
pub struct Pnt {
    pub x: i64,
    pub y: i64,
}

#[derive(Clone, Copy, Default, Debug)]
pub struct Rect {
    pub pnt: Pnt,
    pub dim: Dim,
}

impl Rect {
    pub fn from_lrtp(left: i64, right: i64, top: i64, bottom: i64) -> Self {
        Rect {
            pnt: Pnt { x: left, y: top },
            dim: Dim {
                dx: right - left,
                dy: bottom - top,
            },
        }
    }

    pub fn from_wh(width: i64, height: i64) -> Self {
        Rect {
            pnt: Pnt { x: 0, y: 0 },
            dim: Dim {
                dx: width,
                dy: height,
            },
        }
    }

    pub fn left(&self) -> i64 {
        self.pnt.x
    }
    pub fn right(&self) -> i64 {
        self.pnt.x + self.dim.dx
    }
    pub fn top(&self) -> i64 {
        self.pnt.y
    }
    pub fn bottom(&self) -> i64 {
        self.pnt.y + self.dim.dy
    }
    // pub fn center(&self) -> Pnt {
    //     Pnt {
    //         x: self.pnt.x + self.dim.dx / 2,
    //         y: self.pnt.y + self.dim.dy / 2,
    //     }
    // }

    pub fn cut_padding(&self, left: i64, right: i64, top: i64, bottom: i64) -> Self {
        Self::from_lrtp(
            self.left() + left,
            self.right() - right,
            self.top() + top,
            self.bottom() - bottom,
        )
    }

    // TODO: consider (&mut self, offset: i64) -> Self again
    pub fn cut_from_left(&self, offset: i64) -> (Self, Self) {
        let left = Rect {
            pnt: self.pnt,
            dim: Dim {
                dx: offset,
                dy: self.dim.dy,
            },
        };
        let right = Rect {
            pnt: Pnt {
                x: self.pnt.x + offset,
                y: self.pnt.y,
            },
            dim: Dim {
                dx: self.dim.dx - offset,
                dy: self.dim.dy,
            },
        };

        (left, right)
    }

    pub fn cut_from_top(&self, offset: i64) -> (Self, Self) {
        let top = Rect {
            pnt: self.pnt,
            dim: Dim {
                dx: self.dim.dx,
                dy: offset,
            },
        };
        let bottom = Rect {
            pnt: Pnt {
                x: self.pnt.x,
                y: self.pnt.y + offset,
            },
            dim: Dim {
                dx: self.dim.dx,
                dy: self.dim.dy - offset,
            },
        };

        (top, bottom)
    }

    pub fn cut_from_right(&self, offset: i64) -> (Self, Self) {
        let left = Rect {
            pnt: self.pnt,
            dim: Dim {
                dx: self.dim.dx - offset,
                dy: self.dim.dy,
            },
        };
        let right = Rect {
            pnt: Pnt {
                x: self.pnt.x + self.dim.dx - offset,
                y: self.pnt.y,
            },
            dim: Dim {
                dx: offset,
                dy: self.dim.dy,
            },
        };

        (left, right)
    }

    pub fn cut_from_bottom(&self, offset: i64) -> (Self, Self) {
        let top = Rect {
            pnt: self.pnt,
            dim: Dim {
                dx: self.dim.dx,
                dy: self.dim.dy - offset,
            },
        };
        let bottom = Rect {
            pnt: Pnt {
                x: self.pnt.x,
                y: self.pnt.y + self.dim.dy - offset,
            },
            dim: Dim {
                dx: self.dim.dx,
                dy: offset,
            },
        };

        (top, bottom)
    }

    pub fn center_in(&self, parent: Self) -> Self {
        Rect {
            pnt: Pnt {
                x: parent.pnt.x + (parent.dim.dx - self.dim.dx) / 2,
                y: parent.pnt.y + (parent.dim.dy - self.dim.dy) / 2,
            },
            dim: self.dim,
        }
    }
}
