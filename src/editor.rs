use crate::bytes;
use crate::drawing;
use crate::geo;
use crate::keys;

const COL_MAGENTA: u32 = 0xFFfd5ff0;

const COL_BACKGROUND: u32 = 0xFF062329;
const COL_TEXT: u32 = 0xFFd1b897;
const COL_HIGHLIGHTLINE: u32 = 0xFF0b3335;
const COL_LINE_FG: u32 = 0xFF126367;

const E_COMMAND_BUFFER_CAP: usize = 64;
const E_TEXT_BUFFER_CAP: usize = 32 * 1024 * 1024;

#[derive(PartialEq)]
pub enum EditorMode {
    Normal,
    Insert,
    Command,
}

pub struct Editor {
    pub is_running: bool,
    mode: EditorMode,

    rune_dim: geo::Dim,
    line_height: usize,

    file_path: Option<String>,

    command_buffer: [u8; E_COMMAND_BUFFER_CAP],
    command_buffer_used: usize,
    command_buffer_cursor: usize,

    text_buffer: Vec<u8>,
    text_buffer_used: usize,

    scroll_offset: usize,
    scroll_line: usize,
    // scroll_column: usize,
    cursor_offset: usize,
    cursor_line: usize,
    cursor_column: usize,
}

impl Editor {
    pub fn new() -> Self {
        Editor {
            is_running: true,
            mode: EditorMode::Normal,
            rune_dim: geo::Dim { dx: 0, dy: 0 },
            line_height: 0,

            file_path: None,

            command_buffer: [b'\0'; E_COMMAND_BUFFER_CAP],
            command_buffer_used: 0,
            command_buffer_cursor: 1,

            text_buffer: vec![b'\0'; E_TEXT_BUFFER_CAP],
            text_buffer_used: 0,

            scroll_offset: 0,
            scroll_line: 0,
            // scroll_column: 0,
            cursor_offset: 0,
            cursor_line: 0,
            cursor_column: 0,
        }
    }

    fn cursor_move_up(&mut self) {
        if self.cursor_line == 0 {
            return;
        }

        let prev_offset = self.cursor_offset - self.cursor_column - 1;
        let prev_line_len = self.get_line(prev_offset).len();
        self.cursor_line -= 1;
        self.cursor_column = std::cmp::min(self.cursor_column, prev_line_len);
        self.cursor_offset = prev_offset - prev_line_len + self.cursor_column;
    }

    fn cursor_move_down(&mut self) {
        let line_len = self.get_line(self.cursor_offset).len();
        let next_offset = self.cursor_offset + 1 + line_len - self.cursor_column;
        if next_offset >= self.text_buffer_used {
            return;
        }

        let next_line_len = self.get_line(next_offset).len();
        self.cursor_line += 1;
        self.cursor_column = std::cmp::min(self.cursor_column, next_line_len);
        self.cursor_offset = next_offset + self.cursor_column;
    }

    fn cursor_move_left(&mut self) {
        if self.cursor_offset == 0 {
            return;
        }
        if self.cursor_column == 0 {
            let prev_line_len = self.get_line(self.cursor_offset - 1).len();
            self.cursor_line -= 1;
            self.cursor_column = prev_line_len;
        } else {
            self.cursor_column -= 1;
        }
        self.cursor_offset -= 1;
    }

    fn cursor_move_right(&mut self) {
        if self.cursor_offset == self.text_buffer_used {
            return;
        }
        let line_len = self.get_line(self.cursor_offset).len();
        if self.cursor_column == line_len {
            self.cursor_line += 1;
            self.cursor_column = 0;
        } else {
            self.cursor_column += 1;
        }
        self.cursor_offset += 1;
    }

    fn cursor_handle_event(&mut self, uni_char: u8) {
        match uni_char {
            b'k' | keys::UP => self.cursor_move_up(),
            b'j' | keys::DOWN => self.cursor_move_down(),
            b'h' | keys::LEFT => self.cursor_move_left(),
            b'l' | keys::RIGHT => self.cursor_move_right(),
            _ => {}
        }
    }

    fn normal_handle_event(&mut self, uni_char: u8) {
        match uni_char {
            b'i' => {
                self.mode = EditorMode::Insert;
            }
            b':' => {
                self.command_buffer_reset();
                self.mode = EditorMode::Command;
            }
            _ => self.cursor_handle_event(uni_char),
        }
    }

    fn insert_handle_event(&mut self, uni_char: u8) {
        match uni_char {
            b'\r' | b'\n' => {
                self.insert(b'\n');
                self.cursor_move_right();
            }
            c if 32 <= c && c < 128 => {
                self.insert(uni_char);
                self.cursor_move_right();
            }
            keys::ESC => {
                self.mode = EditorMode::Normal;
            }
            keys::BACKSPACE => {
                self.cursor_move_left();
                self.remove();
            }
            _ => self.cursor_handle_event(uni_char),
        }
    }

    fn command_execute(&mut self) -> bool {
        match self.command_buffer_used {
            2 => match self.command_buffer[1] {
                b'q' => {
                    self.is_running = false;
                    true
                }
                b'w' => match &self.file_path {
                    Some(file_path) => {
                        match std::fs::write(
                            &file_path,
                            &self.text_buffer[0..self.text_buffer_used],
                        ) {
                            Ok(()) => true,
                            _ => false,
                        }
                    }
                    _ => false,
                },
                _ => false,
            },
            used if used > 3 => match self.command_buffer[1] {
                b'e' => {
                    let file_path_raw = &self.command_buffer[3..used];
                    let file_path = std::str::from_utf8(file_path_raw).unwrap();
                    match std::fs::read(file_path) {
                        Ok(file_content) => {
                            self.file_path = Some(file_path.to_string());
                            let len = std::cmp::min(E_TEXT_BUFFER_CAP, file_content.len());
                            let mut used = 0;
                            for i in 0..len {
                                if file_content[i] != 13 {
                                    self.text_buffer[used] = file_content[i];
                                    used += 1;
                                }
                            }
                            self.text_buffer[used] = b'\n';
                            self.text_buffer_used = used;
                            self.cursor_offset = 0;
                            self.cursor_line = 0;
                            self.cursor_column = 0;
                            true
                        }
                        _ => false,
                    }
                }
                _ => false,
            },
            _ => false,
        }
    }

    fn command_buffer_insert(&mut self, uni_char: u8) {
        if self.command_buffer_used >= E_COMMAND_BUFFER_CAP {
            return;
        }

        for i in self.command_buffer_cursor..self.command_buffer_used {
            let idx = self.command_buffer_used - 1 - i;
            self.command_buffer[idx + 1] = self.command_buffer[idx];
        }

        self.command_buffer[self.command_buffer_cursor] = uni_char;
        self.command_buffer_used += 1;
        self.command_buffer_cursor += 1;
    }

    fn command_buffer_remove(&mut self) {
        if self.command_buffer_used <= 1 {
            return;
        }

        self.command_buffer_used -= 1;
        self.command_buffer_cursor -= 1;

        for i in self.command_buffer_cursor..self.command_buffer_used {
            self.command_buffer[i] = self.command_buffer[i + 1];
        }
        self.command_buffer[self.command_buffer_used] = b'\0';
    }

    fn command_buffer_reset(&mut self) {
        self.command_buffer[0] = b':';
        self.command_buffer_used = 1;
        self.command_buffer_cursor = 1;
    }

    fn command_handle_event(&mut self, uni_char: u8) {
        match uni_char {
            b'\r' | b'\n' => {
                if self.command_execute() {
                    self.mode = EditorMode::Normal;
                }
            }
            c if 32 <= c && c < 128 => {
                self.command_buffer_insert(uni_char);
            }
            keys::ESC => {
                self.mode = EditorMode::Normal;
            }
            keys::BACKSPACE => {
                self.command_buffer_remove();
            }
            keys::LEFT => {
                if self.command_buffer_cursor > 1 {
                    self.command_buffer_cursor -= 1;
                }
            }
            keys::RIGHT => {
                if self.command_buffer_cursor < self.command_buffer_used {
                    self.command_buffer_cursor += 1;
                }
            }
            _ => {}
        }
    }

    pub fn handle_event(&mut self, uni_char: u8) {
        match self.mode {
            EditorMode::Normal => {
                self.normal_handle_event(uni_char);
            }
            EditorMode::Insert => {
                self.insert_handle_event(uni_char);
            }
            EditorMode::Command => {
                self.command_handle_event(uni_char);
            }
        }
    }

    fn get_line(&self, offset: usize) -> &[u8] {
        let offset = if self.text_buffer_used <= offset {
            self.text_buffer_used - 1
        } else {
            offset
        };

        let (line_start, line_end) = bytes::bounds_of(&self.text_buffer, offset, b'\n');
        &self.text_buffer[line_start..line_end]
    }

    fn insert(&mut self, byte: u8) {
        let cursor = self.cursor_offset;
        let used = self.text_buffer_used;

        if used < cursor || E_TEXT_BUFFER_CAP <= used {
            return;
        }

        bytes::move_values(&mut self.text_buffer, cursor + 1, used + 1, cursor, used);
        self.text_buffer[cursor] = byte;
        self.text_buffer_used += 1;
    }

    fn remove(&mut self) {
        let cursor = self.cursor_offset;
        let used = self.text_buffer_used;

        if used <= cursor {
            return;
        }

        bytes::move_values(&mut self.text_buffer, cursor, used - 1, cursor + 1, used);
        self.text_buffer[used] = b'\n';
        self.text_buffer_used -= 1;
    }

    pub fn draw(&mut self, img: &mut drawing::Image, bounds: geo::Rect) {
        self.rune_dim = drawing::measure_text("H");
        self.line_height = self.rune_dim.dx as usize + 4 + 4;

        let (top, bottom) = bounds.cut_from_bottom(24);

        self.draw_text_area(img, top);
        self.draw_status_bar(img, bottom);
    }

    fn draw_line_number(&self, img: &mut drawing::Image, b: geo::Rect, line_number: usize) {
        let bounds = b.cut_padding(4, 0, 2, 2);
        let text = format!("{:03}", line_number);
        img.draw_text(&text, bounds.pnt, COL_LINE_FG);
    }

    fn draw_text_area(&mut self, img: &mut drawing::Image, bounds: geo::Rect) {
        let (mut line_number_bounds, mut text_bounds) =
            bounds.cut_from_left(3 * self.rune_dim.dx + 8);
        img.fill_rect(line_number_bounds, COL_HIGHLIGHTLINE);
        img.fill_rect(text_bounds, COL_BACKGROUND);

        if self.file_path.is_none() {
            let text = "editor";
            let dim = drawing::measure_text(&text);
            let rect = geo::Rect {
                pnt: geo::Pnt { x: 0, y: 0 },
                dim: dim,
            }
            .center_in(text_bounds);
            img.draw_text(&text, rect.pnt, COL_TEXT);
            return;
        }

        text_bounds = text_bounds.cut_padding(2, 2, 2, 2);
        let bytes = &self.text_buffer[0..self.text_buffer_used];
        // TODO: move to own fn ensure_cursor_in_scroll
        let max_lines = (text_bounds.dim.dy as usize) / self.line_height;
        if self.cursor_line < self.scroll_line {
            self.scroll_offset = self.cursor_offset - self.cursor_column;
            self.scroll_line = self.cursor_line;
        }
        while self.cursor_line > (self.scroll_line + max_lines - 1) {
            self.scroll_offset = bytes::next_of(bytes, self.scroll_offset, b'\n') + 1;
            self.scroll_line += 1;
        }

        let mut line_offset = self.scroll_offset;
        let mut line_number = self.scroll_line;
        while line_offset < self.text_buffer_used && line_number < (self.scroll_line + max_lines) {
            let (line_bounds, lnb) = line_number_bounds.cut_from_top(self.line_height as i64);
            line_number_bounds = lnb;
            self.draw_line_number(img, line_bounds, line_number + 1);

            let line = self.get_line(line_offset);

            let (cursor_bounds, b) = text_bounds.cut_from_top(self.line_height as i64);
            text_bounds = b;
            let line_bounds = cursor_bounds.cut_padding(2, 2, 2, 2);

            if line_number == self.cursor_line && self.mode != EditorMode::Command {
                if self.mode == EditorMode::Normal {
                    img.fill_rect(cursor_bounds, COL_HIGHLIGHTLINE);
                }
                let x = (self.cursor_column as i64) * self.rune_dim.dx + 1;
                let mut cursor = cursor_bounds;
                cursor.pnt.x += x;
                cursor.dim.dx = if self.mode == EditorMode::Insert {
                    1
                } else {
                    self.rune_dim.dx + 2
                };
                img.draw_rect(cursor, COL_TEXT);
            }

            img.draw_text(
                &std::str::from_utf8(line).unwrap(),
                line_bounds.pnt,
                COL_TEXT,
            );

            line_offset += line.len() + 1;
            line_number += 1;
        }
    }

    fn draw_status_bar(&self, img: &mut drawing::Image, b: geo::Rect) {
        img.fill_rect(b, COL_LINE_FG);

        let padding = (b.dim.dy - self.rune_dim.dy) / 2;
        let main_bounds = b.cut_padding(padding, padding, padding - 2, padding - 2);
        let bounds = main_bounds.cut_padding(0, 0, 2, 2);

        match self.mode {
            EditorMode::Normal => {
                img.draw_text("Normal", bounds.pnt, COL_TEXT);
            }
            EditorMode::Insert => {
                img.draw_text("Insert", bounds.pnt, COL_MAGENTA);
            }
            EditorMode::Command => {
                let (command_bounds, _right) =
                    main_bounds.cut_from_left(self.rune_dim.dx * E_COMMAND_BUFFER_CAP as i64);

                img.fill_rect(command_bounds, COL_HIGHLIGHTLINE);

                let x = self.command_buffer_cursor as i64 * self.rune_dim.dx;
                let mut cursor = command_bounds;
                cursor.pnt.x += x;
                cursor.dim.dx = 1;
                img.draw_rect(cursor, COL_TEXT);

                let text =
                    std::str::from_utf8(&self.command_buffer[0..self.command_buffer_used]).unwrap();
                img.draw_text(&text, command_bounds.cut_padding(0, 0, 2, 2).pnt, COL_TEXT);
            }
        }

        if self.file_path.is_some() {
            let file_path = self.file_path.as_ref().unwrap();
            let text = format!(
                "{}:{}:{}:{}",
                file_path,
                self.cursor_line + 1,
                self.cursor_column + 1,
                self.cursor_offset
            );
            let dim = drawing::measure_text(&text);
            let (_left, bounds) = bounds.cut_from_right(dim.dx);
            img.draw_text(&text, bounds.pnt, COL_TEXT);
        }
    }
}
