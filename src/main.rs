use minifb;
use std;

mod bytes;
mod drawing;
mod editor;
mod geo;
mod keys;

const WIDTH: usize = 1280;
const HEIGHT: usize = 720;

// -------------------------------------------
// Hacky event handling
static mut INPUT_COUNT: usize = 0;
static mut INPUT: [u8; 32] = [0; 32];
struct Callback {}
impl minifb::InputCallback for Callback {
    fn add_char(&mut self, uni_char: u32) {
        if uni_char < 128 {
            unsafe {
                INPUT[INPUT_COUNT] = uni_char as u8;
                INPUT_COUNT = INPUT_COUNT + 1;
            }
        }
    }
}
// -------------------------------------------

fn main() {
    let mut editor = editor::Editor::new();
    let mut frame_buffer = drawing::Image {
        dim: geo::Dim {
            dx: WIDTH as i64,
            dy: HEIGHT as i64,
        },
        buffer: vec![0; WIDTH * HEIGHT],
    };

    let mut window = minifb::Window::new("Editor", WIDTH, HEIGHT, minifb::WindowOptions::default())
        .unwrap_or_else(|e| {
            panic!("{}", e);
        });

    let cb = Box::new(Callback {});
    window.set_input_callback(cb);
    window.set_key_repeat_delay(0.1);
    window.limit_update_rate(Some(std::time::Duration::from_micros(16600)));

    while window.is_open() && editor.is_running {
        window
            .get_keys_pressed(minifb::KeyRepeat::Yes)
            .map(|keys_pressed| {
                for t in keys_pressed {
                    let key = match t {
                        minifb::Key::Up => keys::UP,
                        minifb::Key::Down => keys::DOWN,
                        minifb::Key::Left => keys::LEFT,
                        minifb::Key::Right => keys::RIGHT,
                        _ => 0,
                    };
                    if key > 0 {
                        unsafe {
                            INPUT[INPUT_COUNT] = key;
                            INPUT_COUNT = INPUT_COUNT + 1;
                        }
                    }
                }
            });

        let input_count = unsafe { INPUT_COUNT };
        for i in 0..input_count {
            let uni_char = unsafe { INPUT[i] };
            editor.handle_event(uni_char);
        }
        unsafe {
            INPUT_COUNT = 0;
        }

        let frame_bounds = geo::Rect::from_wh(WIDTH as i64, HEIGHT as i64);

        for i in frame_buffer.buffer.iter_mut() {
            *i = 0xFFFF00FF;
        }

        editor.draw(&mut frame_buffer, frame_bounds);

        window
            .update_with_buffer(&frame_buffer.buffer, WIDTH, HEIGHT)
            .unwrap();
    }
}
